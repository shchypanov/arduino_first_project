import React from 'react';

export default function Hero() {
  return (
    <section className="hero">
      <div className="hero-body">
        <div className="container">
          <img src="arduino.jpg" alt="" />
        </div>
      </div>
    </section>
  )
}
