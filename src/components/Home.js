import React, { Fragment } from 'react';
import Hero from './Hero';

export default function Home() {
  return (
    <Fragment>
      <Hero />
      <div className="box cta">
        <p className="has-text-centered">
          Hey guys! Here we can check AWS IoT functionality. We just gonna send simple string to our arduino device. Just try it on Admin Tabs!
        </p>
      </div>
    </Fragment>
  )
}
