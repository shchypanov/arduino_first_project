import React, { Component } from 'react';
import AWS from 'aws-sdk';
const config = require('../config.json');

export default class Bonus extends Component {
  onChangeValue(event) {
    const lambda = new AWS.Lambda({
      region: config.aws.region,
      accessKeyId: config.aws.accessKeyId,
      secretAccessKey: config.aws.secretAccessKey
    });

    const params = {
      FunctionName: config.aws.iotLightEndpoint,
      Payload: JSON.stringify({ color: event.target.value }),
    };

    lambda.invoke(params, function(err, data) {
      if (err) console.log(err, err.stack);
      else     console.log(data);
    });

  }

  render() {
    return (
      <div className="block">
        <h2 className="has-text-centered">Please, choose any Light-emitting diode color:</h2>
        <div onChange={this.onChangeValue} className="control has-text-centered">
          <label className="radio">
            <input type="radio" value="red" name="color" /> Red
          </label>
          <label className="radio">
            <input type="radio" value="green" name="color" /> Green
          </label>
          <label className="radio">
            <input type="radio" value="blue" name="color" /> Blue
          </label>
        </div>
      </div>
    )
  }
}
